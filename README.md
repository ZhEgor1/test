Test task for the position of trainee android developer.

Structure:
The application consists of two screens:
 First screen functionality:
  Displaying gif images as a table
 Functionality of the second screen:
  Full screen display of the selected GIF

The Retrofit library was used to execute requests to the api.

Using:
 API 26: Android 8.0 (Oreo)