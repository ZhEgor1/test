package com.example.test.data.remote.common

data class ImagesGif(var downsized: DownsizedGif, var original: OriginalGif) {
    fun copy(imagesGif: ImagesGif) {
        downsized.copy(imagesGif.downsized)
        original.copy(imagesGif.original)
    }
}