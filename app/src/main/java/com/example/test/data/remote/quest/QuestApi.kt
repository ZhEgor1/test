package com.example.test.data.remote.quest

import com.example.test.data.remote.common.GiphyGifDataList
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers

interface QuestApi {
    @GET("./search?api_key=rodehM9pZxXg8QyoplIRiBjssWv3YMbp&q=cats&limit=13&offset=0&rating=g&lang=en")
    @Headers("Content-Type: application/json")
    fun getQuestList(): Single<GiphyGifDataList>

    @GET("./search?api_key=rodehM9pZxXg8QyoplIRiBjssWv3YMbp&q=cats&limit=13&offset=0&rating=g&lang=en")
    @Headers("Content-Type: application/json")
    fun getQuestString(): Single<String>
}