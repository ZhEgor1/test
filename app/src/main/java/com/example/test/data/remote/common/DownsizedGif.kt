package com.example.test.data.remote.common

data class DownsizedGif(var url: String) {
    fun copy(downsizedGif: DownsizedGif) {
        url = downsizedGif.url
    }
}