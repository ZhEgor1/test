package com.example.test.data.remote.common

data class GiphyGifData(var images: ImagesGif) {
    fun copy(giphyGifData: GiphyGifData) {
        images.copy(giphyGifData.images)
    }
}