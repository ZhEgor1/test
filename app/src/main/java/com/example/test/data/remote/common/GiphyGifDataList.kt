package com.example.test.data.remote.common

data class GiphyGifDataList(var data: MutableList<GiphyGifData> = mutableListOf()) {
    fun copy(copyGiphyGifDataList: GiphyGifDataList) {
        for (i: Int in 0 until copyGiphyGifDataList.data.count()) {
            data.add(GiphyGifData(ImagesGif(DownsizedGif(""), OriginalGif(""))))
            data[i].copy(copyGiphyGifDataList.data[i])
        }
    }
}

//(GiphyGifData(ImagesGif(DownsizedGif(""), OriginalGif(""))))