package com.example.test.data.remote.common

data class OriginalGif(var url: String) {
    fun copy(originalGif: OriginalGif) {
        url = originalGif.url
    }
}