package com.example.test.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.test.R

class ViewerActivity : AppCompatActivity(), View.OnClickListener {
    private var currentIndex: Int = 0
    private lateinit var img: ImageView
    private lateinit var arrayList: ArrayList<String>
    private lateinit var btnPrev: Button
    private lateinit var btnNext: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_viewer)
        val incomingIntent = intent
        arrayList = incomingIntent.getStringArrayListExtra("arrayList") as ArrayList<String>
        currentIndex = incomingIntent.getIntExtra("currentIndex", 0)
        Log.e("TAG", "onCreate")
        init()
        pagingCheck()
        uploadGIF(img, currentIndex)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("currentIndex", currentIndex)
        Log.e("TAG", "onSave")

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        currentIndex = savedInstanceState.getInt("currentIndex")
        Log.e("TAG", "onRestore")
        pagingCheck()
        uploadGIF(img, currentIndex)
    }

    private fun pagingCheck() {
        if (currentIndex == 0 && currentIndex == arrayList.count() - 1) {
            btnPrev.visibility = View.INVISIBLE
            btnNext.visibility = View.INVISIBLE
        } else if (currentIndex == 0) {
            btnPrev.visibility = View.INVISIBLE
            btnNext.visibility = View.VISIBLE
        } else if (currentIndex == arrayList.count() - 1){
            btnPrev.visibility = View.VISIBLE
            btnNext.visibility = View.INVISIBLE
        } else {
            btnPrev.visibility = View.VISIBLE
            btnNext.visibility = View.VISIBLE
        }
    }
    private fun init() {
        btnPrev = findViewById(R.id.btnPrev)
        btnPrev.setOnClickListener(this)
        btnNext = findViewById(R.id.btnNext)
        btnNext.setOnClickListener(this)
        img = findViewById(R.id.imageView)
    }
    private fun uploadGIF(iv: ImageView, index: Int) {
        Glide.with(this)
            .load(arrayList[index])
            .into(iv)
        Log.e("TAG", index.toString() + "\t" + arrayList[index])
    }
    override fun onClick(v: View?) {
        when (v?.id){
            R.id.btnPrev->{
                currentIndex--
                pagingCheck()
                uploadGIF(img, currentIndex)
            }
            R.id.btnNext ->{
                currentIndex++
                pagingCheck()
                uploadGIF(img, currentIndex)
            }
        }

    }
}