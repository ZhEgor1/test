package com.example.test.screens

import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TableLayout
import android.widget.TableRow
import com.bumptech.glide.Glide
import com.example.test.QuestApp
import com.example.test.R
import com.example.test.data.remote.common.GiphyGifDataList
import com.example.test.data.remote.quest.QuestApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlin.math.floor

class MainActivity : AppCompatActivity() {
    private val compositeDisposable = CompositeDisposable()
    private val giphyGifDataList = GiphyGifDataList()
    private lateinit var table: TableLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fetchQuestList((this.application as QuestApp).questApi)
        init()
    }

    private fun fetchQuestList(questApi: QuestApi?) {
        questApi?.let {
            compositeDisposable.add(questApi.getQuestList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    giphyGifDataList.copy(it)
                    fillTable()
                },{

                })
            )
        }
    }

    private fun init() {
        table = findViewById(R.id.table)
    }
    private fun fillTable() {

        var countColumn: Int
        countColumn = if (Configuration.ORIENTATION_PORTRAIT == resources.configuration.orientation) 2 else 4

        Log.e("TAG", giphyGifDataList.data.size.toString())
        val countRow = floor((giphyGifDataList.data.count().toDouble()/countColumn.toDouble())).toInt()
        var item = 0
        for (row: Int in 0..countRow) {
            table.addView(TableRow(this))
            if (giphyGifDataList.data.count() - (giphyGifDataList.data.count() % countColumn) == item)
                countColumn = giphyGifDataList.data.count() % countColumn
            for (column: Int in 0 until countColumn) {
                val tr: TableRow = table.getChildAt(row) as TableRow
                tr.gravity = 17
                val iv = ImageView(this)
                iv.tag = item
                iv.setOnClickListener {
                    intent = Intent(this, ViewerActivity::class.java)
                    intent.putExtra("currentIndex", iv.tag.toString().toInt())
                    intent.putExtra("arrayList", convertToArrayList())

                    startActivity(intent)
                }
                uploadGIF(iv, giphyGifDataList.data[item].images.downsized.url)
                tr.addView(iv, 500, 800)
                item++
            }
        }
    }

    private fun uploadGIF(iv: ImageView, url: String) {
        Glide.with(this)
            .load(url)
            .into(iv)
    }

    private fun convertToArrayList(): ArrayList<String> {
        val gifsOrigURL: ArrayList<String> = arrayListOf()
        for (item in giphyGifDataList.data)
            gifsOrigURL.add(item.images.original.url)
        return gifsOrigURL
    }
}